#!/bin/bash

# API related.
export API_HOST="https://staging-fuel.ignitionrobotics.org"
export API_VERSION="1.0"
export CLOUDSIM_HOST="https://staging-cloudsim.ignitionrobotics.org"
export CLOUDSIM_VERSION="1.0"

# Auth0 related.
export AUTH0_AUDIENCE="https://staging-api.ignitionfuel.org"
export AUTH0_CLIENT_DOMAIN="ignitionrobotics-staging.auth0.com"
export AUTH0_CLIENT_ID="MjT19rUiv1LX3JCMXcaaYdKennuikPFw"
export AUTH0_LOGOUT_REDIRECT="https://staging-app.ignitionrobotics.org"
export AUTH0_REDIRECT="https://staging-app.ignitionrobotics.org/callback"

# Misc
export AWS_GZ_LOGS_BUCKET="web-cloudsim-staging-logs"
export SUBT_PORTAL_URL="https://staging.subtchallenge.world"
